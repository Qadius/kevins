import React, { Component } from 'react';
import { Container } from 'reactstrap';

import './css/App.css';
import './css/Table.css';
import Topbar from './Components/Navigation/Topbar';

// import some hardcoded data
import { items as databaseItems } from './HardcodedData';
import Item from './Components/Item';
import Filter from './Components/Filter';

class App extends Component {
  constructor(props) {
    super(props);

    this.filter = this.filter.bind(this);

    this.state = {
      items: [],
      loading: true
    }
  }

  async componentWillMount() {
    // this would be where you might make an initial fetch call to retrieve some initial items,
    // then, inside a .then() do the following
    var data = databaseItems;
    this.setState({
      items: data,
      loading: false
    });
  }

  filter(filterObject) {
    // this is where you would make a fetch call to retrieve a list of items based on the user's filter, and NOT make a chain of if statements instead...
    var items = databaseItems;

    // perform a nullcheck of each filter to ensure it's not ""
    if (filterObject.name) {
      // filter the list of items based on the name input
      items = items.filter(i => i.name.toLowerCase().includes(filterObject.name.toLowerCase()));
    }
    if (filterObject.number) {
      // you could probably also handle GT or LT queries maybe?
      items = items.filter(i => i.number === filterObject.number);
    }
    if (filterObject.description) {
      items = items.filter(i => i.description.toLowerCase().includes(filterObject.description.toLowerCase()));
    }
    if (filterObject.type) {
      items = items.filter(i => i.type.toLowerCase() === filterObject.type.toLowerCase());
    }

    // apply the filter
    this.setState({ items: items });
  }

  render() {
    return (
      <Container>
        <Topbar />
        <Filter filter={this.filter} />
        {/* We don't use a bootstrap table here because HTML tables do not like being expanded/contracted */}
        <div className="items">
          {this.state.items.map((item) => {
            return <Item key={item.name} item={item} />
          })}
        </div>
      </Container>
    );
  }
}

export default App;
