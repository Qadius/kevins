export const items = [
    {
        name: "A ridiculously adorable kitten",
        type: "kitten",
        number: 1,
        description: "A good item",
        label: "Item",
        image: "https://www.catster.com/wp-content/uploads/2017/12/A-gray-kitten-meowing.jpg"
    },
    {
        name: "A much less adorable kitten",
        type: "kitten",
        number: 2,
        description: "Another good item",
        label: "ItemP",
        image: "https://kittenrescue.org/wp-content/uploads/2017/03/KittenRescue_KittenCareHandbook.jpg"
    },
    {
        name: "A loving puppy",
        type: "puppy",
        number: 100,
        description: "Another, even better, good item",
        label: "Itemzz",
        image: "https://cdn2-www.dogtime.com/assets/uploads/gallery/30-impossibly-cute-puppies/impossibly-cute-puppy-15.jpg"
    },
    {
        name: "Four-legged asshole with tail",
        type: "puppy",
        number: 102,
        description: "The very last item I made",
        label: "Nowaybro",
        image: "https://cdn2-www.dogtime.com/assets/uploads/gallery/30-impossibly-cute-puppies/impossibly-cute-puppy-10.jpg"
    }
]