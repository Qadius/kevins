import React, { Component } from 'react';
import { 
    Col, 
    Row, 
    Button, 
    Form, 
    FormGroup, 
    Input 
} from 'reactstrap';

export default class Filter extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);

        this.state = {
            type: "",
            number: "",
            name: "",
            description: ""
        }
    }

    handleSubmit() {
        // we call the filter function passed down by the parent.
        // this makes it reusable as we can specify a custom function whenever we use this component (assuming the parent can parse the filter object)
        // it also keeps this component focused on the form and user input exclusively, and leaves interaction
        // with our REST API to a component higher up the food chain
        this.props.filter(this.state);

        // reset the form
        this.setState({
            type: "",
            number: "",
            name: "",
            description: ""
        })
    }

    handleChange(e) {
        // the below works because we specified the name of all of the input elements
        this.setState({ [e.target.name]: e.target.value });
    }

    handleChangeType(e) {
        // handle a reseat of the type filter (i.e. user clicked "Puupies" and then decided they dont want to filter by type at all)
        if (e.target.value === "Pet Type") {
            this.setState({ type: "" });
        } else {
            this.setState({ type: e.target.value });
        }
    }

    render() {
        console.log(this.state)

        return (
            <Form className="filter">
                <Row>
                    <Col md={2}>
                        <FormGroup>
                            <Input type="select"
                                name="type"
                                onChange={this.handleChangeType}>
                                <option>Pet Type</option>
                                <option>Puppy</option>
                                <option>Kitten</option>
                            </Input>
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Input
                                type="number"
                                name="number"
                                placeholder="Number"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Input
                                type="text"
                                name="name"
                                placeholder="Name"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Input
                                type="text"
                                name="description"
                                placeholder="Description"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={1}>
                        <Button color="prmary" onClick={this.handleSubmit}>
                            Search
                        </Button>
                    </Col>
                </Row>
            </Form>
        )
    }
}