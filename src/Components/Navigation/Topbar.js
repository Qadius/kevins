import React from 'react';
import {
  Navbar,
  NavbarBrand,
} from 'reactstrap';

export default () => (
  <Navbar>
    <NavbarBrand href="/">Kevin's App</NavbarBrand>
  </Navbar>
);