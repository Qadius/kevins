import React, { Component } from 'react';
import { Button } from 'reactstrap';

export default class Item extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      expanded: false
    }
  }

  toggle() {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    const item = this.props.item;

    return (
      <div className="item">
        <div className="item-header">
          <h3>{item.number}</h3>
          <h3>{item.name}</h3>
          <Button className="dropdown-button" onClick={this.toggle}>
            <i className="material-icons">{this.state.expanded ? "keyboard_arrow_up" : "keyboard_arrow_down"}</i>
          </Button>
        </div>
        {this.state.expanded ?
          <div className="item-body">
            <img src={item.image} alt="Item picture" />
          </div>
          :
          null
        }
      </div>
    );
  }
}